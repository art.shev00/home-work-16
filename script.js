
//test coment
let n = prompt('Print n - number');

function fib (n){
let f0 = 0, f1 = 1, result = 1;

    if (n < 0){
        for (let i = -2; i >= n; i--){
            result = f0 + f1;
            f0 = -f1;
            f1 = -result;
        }
    }else{
        for (let i = 2; i <= n; i++){
            result = f0 + f1;
            f0 = f1;
            f1 = result;
          }
    }
    return result;
  
  }
  
  console.log(fib(n));